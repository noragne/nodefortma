const app = require('express')()
const http = require('http').Server(app)
const io = require('socket.io')(http)
const sqlite3 =  require('sqlite3').verbose()

const db = new sqlite3.Database('./db/database.db', sqlite3.OPEN_CREATE, (err) => {
  if(err) console.error(err.message)
  console.log('Connected to database')
});




app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html')
})

io.on('connection', function(socket){
  // recieve message from server
  
  socket.on('msg', function(data){
    let res 
    console.log(data == 'n')
      switch(data) {
      case '':
        res = 'Please enter a command'
        break;
      case 'position':
        res = 'Your postion is x and y'
        break;
      case 'n':
        res = "The player go to the north by 1"
        break;
      default:
        res = 'This command is incorrect, please try a correct command'

    }
    
    // send message to client when recieve message from it
    io.emit('msgServer', res)
  })
})

// db.close((err) => {
//   if(err) console.error(err.message)
//   console.log('database disconnected')
// })

http.listen(3000, function(){
  console.log("Started server")
})